---
title: "Best Computing Experiences"
description: "Learn about what I consider to be my best computing experiences as an undergraduate student."
date: 2021-08-18T19:41:31-04:00
lastmod: 2021-08-18T19:41:31-04:00
cover: ""
coverAlt: ""
toc: true
tags: [ best, computing, experience ]
draft: false
---

## My best "in class" computing experience

My best in class computing experience happened in Computer Systems 2. We were told
to collaborate with a classmate on our semester project. I asked my partner to use
[Visual Studio Code][1] and the [Live Share extension][2] which lets users edit the
same project as if it were a Google Doc. The rest of the class was trying to use
the student machine and kept running into issues with Putty and git. Since I use
[Arch Linux][4], I was able to run all of the professor's provided tools on my own
computer instead of the student machine. We had a streamlined programming experience
which no other student matched.

## My best "outside of class" computing experience

My best outside of class computing experience is configuring my workflow. This is not
a single moment, but a collection of moments under the same project. As a freshman I
deployed [Nextcloud][3] on my Linux server and set up my school documents to sync
between my server, laptop, and desktop. I occasionally use the web interface to write
documents and upload files from lab computers. I have also used Linux since I was a kid
so my software stack has been deeply optimized for school work. I distinctly remember a
school night my Junior year when I replaced [Manjaro][5] with [Arch Linux][4] on my
laptop and setup system links to sync program-specific data over Nextcloud to my
similarly configured desktop at home. Configuring my workflow has been my pride and joy
for a long time and I become more enthusiastic with every single change.

[1]: https://code.visualstudio.com/
[2]: https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare
[3]: https://nextcloud.com/
[4]: https://archlinux.org/
[5]: https://manjaro.org/
