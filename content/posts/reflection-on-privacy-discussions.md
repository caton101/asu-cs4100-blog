---
title: "Reflection on Privacy Discussions"
description: ""
date: 2021-09-22T16:19:40-04:00
lastmod: 2021-09-22T16:19:40-04:00
cover: ""
coverAlt: ""
toc: false
tags: [privacy, thoughts]
draft: false
---

Our privacy discussions were fascinating, but I did not hear anything new. I am
well informed on online privacy and security. For the sake of simplicity, I
will divide this reflection into two parts: the class discussion and the TED
talk video.

## Class Discussion

The class discussion did not surprise the class. At I looked at my fellow
classmates, it was as if all of us were hearing the same exact words we
are accustomed to hearing. If I learned anything completely new about privacy,
it is that companies are willing to decorate their privacy policy with happy
colors and images in order to steer the reader's attention away from the
text. I have heard about making the privacy policy vague or hard to find,
but decorating the privacy policy is a new tactic that I have not seen before.

## TED Talk

The TED talk was a two sided coin to me. On one side, the person speaking was
incredibly boring to me and said nothing I didn't already know. On the other
side, I was fascinated by the audience and their reactions to the speaker. Most
people in the audience seemed like they were pulling back from the speaker. It
was if they all mentally agreed that the speaker was crazy and all their
speaking points were made up. I even noticed some of them smiling or trying
not to laugh at the speaker when they were reporting on some very serious
claims. If anything, the TED talk showed me how little people care about their
privacy.
