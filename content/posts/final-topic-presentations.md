---
title: "Final Topic Presentations"
description: ""
date: 2021-10-28T14:07:34-04:00
lastmod: 2021-10-28T14:07:34-04:00
cover: ""
coverAlt: ""
toc: false
tags: [final, topic, presentations]
draft: false
---

My favorite presentation was by Kevin Kamto. I like the idea of investigating
why two capitalist countries with different governments have two different
poverty rates. This is a major issue because identifying this relation can help
explain how to address poverty in a way that is also beneficial to the economy.

This topic is also my favorite because it is very relevant in American politics.
Everyone appears to have their own idea of how the country should be ran and if
our government should be replaced with a new one. Finding this link to poverty
means we should avoid a government where one person holds all the power.
