---
title: "Reflection on Ethics Discussion"
description: "My personal thoughts about our in-class ethics discussion"
date: 2021-08-26T14:32:43-04:00
lastmod: 2021-08-26T14:32:43-04:00
cover: ""
coverAlt: ""
toc: false
tags: [ethics, discussion, thoughts]
draft: false
---

I thought the ethics discussion was very fascinating. Ever since I was a small
child, I would discuss any topic I knew about with anyone who was open to
talking. This concept of being open has not changed. I was nervous to share my
ethics since I know college campuses tend to get offended against alternate
opinions. You can imagine my shock when every student openly shared their
ethics and acted like mature adults. I was happier to hear other people's
opinions than I was to share mine.

For our discussion specifically, I was impressed how many of us hold the same
ethics given our diverse belief systems. Not everyone in our class is religious
but a shocking amount of non-religious classmates agreed with the religious
classmates. There were also a lot of people citing Christianity, the
Hippocratic Oath, The Golden Rule, and many other ethical constructs. I was
also somewhat surprised at the number of students who believed in doing
whatever was best for themselves even if it harmed others. I expected this
statistic, but it shocked me nonetheless.

As for improvement, I would love to see an event where we list different ethical
frameworks if we somewhat agree with them. Beliefs are never one-sided and I'm
sure we would all discover that most frameworks hold the same values. It would
also be cool to see how many frameworks we somewhat agree with.

Overall, I'm satisfied with our ethical discussion and hope it will lead to more
interesting discussions in future class sessions.

