---
title: "Final Post"
description: ""
date: 2021-11-17T14:57:09-05:00
lastmod: 2021-11-17T14:57:09-05:00
cover: ""
coverAlt: ""
toc: false
tags: [final, post, thoughts, class, advice]
draft: false
---

This class has been a wild ride with a lot of random discussions in the area of
computer science. To celebrate the end of the semester, I will share the three
most interesting things I have learned and then offer advice to future students.

## Privacy is very endangered

I already knew that privacy was fading away. I saw the Snowden leaks back when
they first dropped and all the conspiracy theorists were bragging about being
correct. As a direct result, I was already keeping myself well informed and
secured my data as much as possible. Now that I realize that most people have
almost no privacy, I have made several changes to my life. I started encrypting
more documents, bought a hardware security key, and cut back on the websites
that I use.

## Crypto is safer than expected

I had experience with Bitcoin in 2013. I ran a CPU miner on an old computer and
collected 0.001 Bitcoin. At the time, crypto was a legal gray area and nobody
knew the legality of mining so I deleted the wallet. That means I have lost
$200 worth of Bitcoin today. To be fair, I did not expect it to take off. Now I
realize crypto is easy to invest in and the popular coins have utility. Most are
even somewhat stable to the point I'm not scared of them crashing.

## Tech socials are very relaxed

This is technically not a part of the class. This was an assignment for outside
of class. I had to attend one Silicon Hollar event. I attended a job fair event
called Work It Watauga. Everyone was drinking and socializing with virtually no
social barriers. I could just walk up to a recruiter that was partly buzzed and
start talking about server administration. I was expecting dress clothes and
formal language and saw street clothes and casual conversation.

## Advice to future students

If you stress about this class, you are doing it wrong. Dr. Wilkes gives
assignments but they do not have an actual due date. The date on AsULearn are
more of a recommendation than an actual requirement. The content of each
assignment is very relaxed and does not take long at all to complete. Consider
this class as a break from normal classes. Sit back. Relax. And enjoy the class.
