---
title: "Thoughts on Blockchain"
description: ""
date: 2021-10-07T16:09:16-04:00
lastmod: 2021-10-07T16:09:16-04:00
cover: ""
coverAlt: ""
toc: false
tags: [thoughts, blockchain, bitcoin, crypto, privacy]
draft: false
---

My thoughts on blockchain are very difficult to convey. I see both pros and cons
to using it. This extends from blockchain the data structure to blockchain the
currency. I will present what I believe to be the pros and cons  below.

## Pros

I think blockchain has a lot of potential for federated tasks like voting and
moderated content. For voting, I appreciate the way each node can "vote" for the
correct chain if a bad block is injected into the chain. This validation was
used in [several states (and even countries) for voting][0] in past elections. I
also see other federated utilities like currency being a very likely path.
Blockchain currency (called cryptocurrency) is making a lot of progress with
rich investors and some countries starting to recognize and regulate it. This
is huge for places where the official currency is inflated or the government is
too invasive in their citizen's activity.

## Cons

The data structure of a blockchain requires too much resources. In order to
participate in a blockchain, you have to process the whole chain. This means you
receive a copy of every block and have to process all the past blocks before you
can start processing the new ones. This operation takes a lot of time,
bandwidth, and storage. This also takes a decent amount of energy before you can
benefit from the blockchain. There are many issues with blockchain and energy on
a large scale. Most blockchain technologies use significantly more energy than
their counterparts.

In terms of security, there is a "majority rule" to voting. When deciding if a
block is legitimate, all nodes will cast a vote if they think the block is
correct. This makes it trivial to inject a bad block in a blockchain. All you
need are a lot of nodes that outnumber all other nodes. For large networks like
bitcoin, this is not affordable for anyone considering the number of computers
you would have to purchase. There are other logistic concerns like storage for
all those computers, the cooling solution, the energy lines, etc. For small
blockchains, these logistic concerns melt away and [you can easily seize
control][1].

[0]: https://hackernoon.com/which-countries-are-casting-voting-using-blockchain-s33j34ab
[1]: https://www.technologyreview.com/2019/02/19/239592/once-hailed-as-unhackable-blockchains-are-now-getting-hacked/

