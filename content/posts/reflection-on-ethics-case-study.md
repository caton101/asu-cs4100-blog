---
title: "Reflection On Hive Tracks and Bee Informed Partnership Ethics Case Study"
description: "a reflection on an ethics case study"
date: 2021-09-02T16:52:09-04:00
lastmod: 2021-09-02T16:52:09-04:00
cover: ""
coverAlt: ""
toc: false
tags: [ethics, "Hive Tracks", "Bee Informed Partnership", bees, thoughts]
draft: false
---

This week we took a deep dive into the world of Hive Tracks and the Bee Informed Partnership. It amazed me how many thoughts and ideas go into a bee tracking database.
For simplicity, I'll divide my thoughts into sections.

## Privacy

I thought the question about a bee's right to privacy was a fascinating discussion. The ethics of this situation could be argued as "for the common good" but it makes me
wonder what our privacy would look like if there was a sentient entity looking over us. The topic of sharing location information was perhaps the most relatable thing
for me since I take location tracking seriously. Of course, I never expected Hive Tracks to have a well-rounded answer since the end user voluntarily gives up location
data when registering a hive. I was glad to hear that there are no cases of hives being stolen because of Hive Tracks sharing its location. Being GDPR compliant is cool
but I wished the data was a little more anonymous. Locations could be randomly skewed and temps could have a slight variation to prevent tracking.

## Security

This is the topic I was most interested in. I was shocked to hear about the webpage being modified. It sounds like Hive Tracks does not prevent honey pots or man in the
middle attacks. As someone who finds exploits interesting, this should be a top priority for the Hive Tracks development team. While the server is secure, there should
be a bigger investment in checking for data breaches to easily detect them. I know specific information about password storage can't be discussed but I hope Hive Tracks
knows not to store plain text passwords in the database.

## Intellectual Property

Being honest, I do not understand intellectual property laws. I write code and do nothing business related. Facebook and other data mining companies have clearly shown
data mining is a profitable business model. From an ethical standpoint, I was upset to hear that data shared with Hive Tracks can be sold and all content produced with
said data can be sold as well. For this reason, I would not personally use Hive Tracks. I prefer open source tools that will not sell data.

