---
title: "Reflection on Citizen Four"
description: "I discuss my opinions of the Citizen Four documentary"
date: 2021-09-21T15:22:38-04:00
lastmod: 2021-09-21T15:22:38-04:00
cover: ""
coverAlt: ""
toc: false
tags: [citizen, four, citizenfour, thoughts, privacy, snowden, nsa]
draft: false
---

My reaction to the documentary was very minimal. I have always been a
well-informed individual when it comes to privacy so I obviously knew about
Snowden and the NSA leaks as a small kid. The only thing that impressed me
about the documentary was the personal aspect. I knew the leaks but it was
interesting to see him portrayed as a regular person with a family and a
girlfriend. I can't say it was anything I didn't expect since I knew what
was going to happen before the documentary mentioned it.

Did it change my thoughts on Snowden? Sort of. I already look up to Snowden
as some sort of privacy role model. His dedication to encryption and leaving
a very small digital footprint is something I inspire to do one day. I already
integrate most of his privacy practices into my life but I want to go even
further (preferably without the leak part). If nothing else, I admire him more
as a person since I was unaware of his personal life before watching the
documentary.

Did it change my thoughts on the US government? Absolutely not. I already give
very little trust to our government. They deserve to be held accountable for
their crimes against humanity but it will never happen since no other country
has the courage to punish them. Like I said, I already knew of the leaks and
the crimes committed by the government. There is nothing new to change my
thoughts. I would like to keep living my life as low profile as possible for
no other reason than to spite the feds. Like 99% of people, I have nothing to
hide but I can pretend I do just to waste their time.

