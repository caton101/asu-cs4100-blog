---
title           : "Contact"
description     : "My contact information."
cover           : "me.png"
coverAlt        : "A picture of me in CGA graphics style."
sitemapExclude  : true
---

That's actually me.

You can find me at:

{{< social >}}

---

Or send me an email:

{{< contact-form >}}
