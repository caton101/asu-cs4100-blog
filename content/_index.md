---
title                 : "Welcome"
disableTitleSeparator : true
---

Welcome to my blog. Check my [posts](/posts) to see my latest content.

Don't like the color scheme? You can pick a different (hopefully better) color by using the colored square in the footer.
