# Blog

This contains the website files for my blog. It uses [Hugo][0] for building
and the [color-your-world][1] theme.

[Here][2] is a link to my blog if anyone is curious.


**NOTE**: This is only for class assignments and should not be taken
seriously. All blog posts will be out of context unless you have taken
the `C S 4100` (Senior Seminar) class at Appalachian State University.

[0]: https://gohugo.io/
[1]: https://gitlab.com/rmaguiar/hugo-theme-color-your-world
[2]: https://cs4100.code-scribe.com
